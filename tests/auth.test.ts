import { app } from "../src/server";
import request from "supertest";

describe("POST user/register", () => {
  it("Should create the user", async () => {
    const res = await request(app).post("/user/register").send({
      email: `testc@gmail.com`,
      password: `password_test`,
      givenName: `Paul`,
      familyName: `Test`,
    });
    expect(res.status).toEqual(200);
  });
  it("Should not create the user because e-mail is already used", async () => {
    const res = await request(app).post("/user/register").send({
      email: `testc@gmail.com`,
      password: `password_test`,
      givenName: `Paul`,
      familyName: `Test`,
    });
    expect(res.body.data.email.msg).toEqual("is already used.");
    expect(res.status).toEqual(500);
  });
  it("Should not create the user because e-mail is missing", async () => {
    const res = await request(app).post("/user/register").send({
      password: `password_test`,
      givenName: `Paul`,
      familyName: `Test`,
    });
    expect(res.body.data.email.msg).toEqual("is missing.");
    expect(res.status).toEqual(400);
  });
});

describe("POST user/login", () => {
  it("Should login the user", async () => {
    const res = await request(app).post("/user/login").send({
      email: `testc@gmail.com`,
      password: `password_test`,
    });
    expect(res.body.data.token).toBeDefined();
    expect(res.status).toEqual(200);
  });
  it("Should not login the user because e-mail is missing", async () => {
    const res = await request(app).post("/user/login").send({
      password: `password_test`,
    });
    expect(res.body.data.email.msg).toEqual("is missing");
    expect(res.status).toEqual(400);
  });
  it("Should not login the user because the user doesn't exist.", async () => {
    const res = await request(app).post("/user/login").send({
      password: `password_test`,
      email: `t@gmail.com`,
    });
    expect(res.body.message).toEqual("Invalid email or password");
    expect(res.status).toEqual(500);
  });
});
