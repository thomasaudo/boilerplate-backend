import * as dotenv from "dotenv";

import { mongoConnect, mongoDisconnect } from "../../src/db";
import { deleteUser } from "../../src/models/user.model";

const deleteTestEnvironment = async () => {
  dotenv.config();

  await mongoConnect();

  await deleteUser(`testa@gmail.com`);
  await deleteUser(`testb@gmail.com`);
  await deleteUser(`testc@gmail.com`);
  await deleteUser(`testd@gmail.com`);
  await deleteUser(`teste@gmail.com`);
  await mongoDisconnect();
};

deleteTestEnvironment();

export { deleteTestEnvironment };
