import * as fs from "fs";
import * as dotenv from "dotenv";
import { promisify } from "util";

import { register } from "../../src/models/auth.model";
import { mongoConnect, mongoDisconnect } from "../../src/db";
import { IUserToCreate } from "../../src/types/user.type";
import { IProjectToCreate } from "../../src/types/project.type";
import { accept, create, invite } from "../../src/models/project.model";
import { deleteUser } from "../../src/models/user.model";

const setUpTestEnvironment = async () => {
  dotenv.config();
  await mongoConnect();

  try {
    let config = {
      jwtToken: ``,
      jwtToken2: ``,
      projectId: ``,
      userId: ``,
      projectIsNotAuthorId: ``,
      projectIsNotInId: ``,
      projectUser2NotInvited: ``,
      userToInviteId: ``,
      jwtTokenUserDeleted: ``,
    };

    let userA: IUserToCreate = {
      givenName: "Thomas",
      email: "testa@gmail.com",
      password: "password_test",
      familyName: "Test",
    };
    const iUserA = await register(userA);
    config.jwtToken = iUserA.token;
    config.userId = iUserA.user._id;
    let userB: IUserToCreate = {
      givenName: "Adrien",
      email: "testb@gmail.com",
      password: "password_test",
      familyName: "Test",
    };
    const iUserB = await register(userB);
    let userC: IUserToCreate = {
      givenName: "Adrien",
      email: "teste@gmail.com",
      password: "password_test",
      familyName: "Test",
    };
    const iUserC = await register(userC);
    config.jwtTokenUserDeleted =iUserC.token;
    await deleteUser("teste@gmail.com");
    config.jwtToken2 = iUserB.token
    let projectA: IProjectToCreate = {
      name: "Projet test A",
      description: "Projet pour les tests jest A",
    };
    config.projectId = (await create(projectA, iUserA.user._id))._id;
    config.projectUser2NotInvited = (await create(projectA, iUserA.user._id))._id;
    let projectB: IProjectToCreate = {
      name: "Projet test B",
      description: "Projet pour les tests jest B",
    };
    config.projectIsNotInId = (await create(projectB, iUserB.user._id))._id;
    config.userToInviteId = iUserB.user._id;
    config.projectIsNotAuthorId = (await create(projectB, iUserB.user._id))._id;
    await invite(config.projectIsNotAuthorId, iUserA.user._id, iUserB.user._id);
    await accept(config.projectIsNotAuthorId, iUserA.user._id);

    const fsWriteFile = promisify(fs.writeFile);

    await fsWriteFile(
      "./tests/environment/data/test-config.json",
      JSON.stringify(config)
    );

    await mongoDisconnect();
  } catch (error) {
    console.log(error);
  }
};

setUpTestEnvironment();

export { setUpTestEnvironment };
