import { mongoConnect, mongoDisconnect } from "../../src/db";
import config from "./data/test-config.json";

beforeAll(async () => {
  await mongoConnect();
});

afterAll(async () => {
  await mongoDisconnect();
});

export { config };
