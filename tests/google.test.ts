import { app } from "../src/server";
import request from "supertest";

describe("GET /user/google/url", () => {
  it("Should retrieve the google Auth url.", async () => {
    const res = await request(app).get("/user/google/url");
    expect(res.body.data.length).toBeGreaterThan(240);
    expect(res.status).toEqual(200);
  });
});

describe("POST /user/google/code", () => {
  it("Should not login/ register the user because the googleCode is missing", async () => {
    const res = await request(app).post("/user/google/code");
    expect(res.body.data.googleCode.msg).toEqual("is missing.");
    expect(res.status).toEqual(400);
  });
  it("Should not login/ register the user because the googleCode is invalid", async () => {
    const invalidCode: string = `invalidCallbackCode`;
    const res = await request(app).post("/user/google/code").send({
      googleCode: invalidCode,
    });
    expect(res.body.message).toEqual("invalid_grant");
    expect(res.status).toEqual(500);
  });
});
