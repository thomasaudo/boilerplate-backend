import { app } from "../src/server";
import request from "supertest";
import * as fs from "fs";

import { config } from "./environment/setup.jest";
const { jwtToken, jwtTokenUserDeleted } = config;

/**
 * TODO: test if user is deleted (jwtTokenUserDeleted)
 */
describe("GET /user/profile", () => {
  it("Should not retrieve the user profile because there is no JWT token.", async () => {
    const res = await request(app).get("/user/profile");
    expect(res.body.message).toEqual(
      "No authorization bearer token header is set."
    );
    expect(res.status).toEqual(401);
  });
  it("Should not retrieve the user profile because the JWT token is invalid.", async () => {
    const badToken: string = `fakeJWT`;
    const res = await request(app)
      .get("/user/profile")
      .set("Authorization", `Bearer ${badToken}`);
    expect(res.body.message).toEqual("Invalid authentication token.");
    expect(res.status).toEqual(401);
  });
  it("Should not retrieve the user profile because the user is deleted.", async () => {
    const badToken: string = `fakeJWT`;
    const res = await request(app)
      .get("/user/profile")
      .set("Authorization", `Bearer ${jwtTokenUserDeleted}`);
    expect(res.body.message).toEqual("User don't exists");
    expect(res.status).toEqual(401);
  });
  it("Should retrieve the user profile.", async () => {
    const res = await request(app)
      .get("/user/profile")
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.data.email).toEqual(`testa@gmail.com`);
    expect(res.status).toEqual(200);
  });
});

describe("POST /user/updatePassword", () => {
  it("Should update the user password.", async () => {
    const res = await request(app)
      .post("/user/updatePassword")
      .send({
        oldPassword: `password_test`,
        newPassword: `newPassword`,
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.data).toEqual(`User's password was changed.`);
    expect(res.status).toEqual(200);
  });
  it("Should not update the user picture because there is no JWT token.", async () => {
    const res = await request(app).post("/user/updatePassword").send({
      oldPassword: `password_test`,
      newPassword: `newPassword`,
    });
    expect(res.body.message).toEqual(
      "No authorization bearer token header is set."
    );
    expect(res.status).toEqual(401);
  });
  it("Should not update the user password because the old password don't match.", async () => {
    const fakePassword: string = `fakePassword`;
    const res = await request(app)
      .post("/user/updatePassword")
      .send({
        oldPassword: fakePassword,
        newPassword: `newPassword`,
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.message).toEqual(`Could not update the user password.`);
    expect(res.status).toEqual(500);
  });
  it("Should not update the user password because the new password is missing.", async () => {
    const res = await request(app)
      .post("/user/updatePassword")
      .send({
        oldPassword: `password_test`,
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.data.newPassword.msg).toEqual(
      `must be of 6 characters length minimum.`
    );
    expect(res.status).toEqual(400);
  });
});

describe("POST /user/updateProfile", () => {
  it("Should update the user profile.", async () => {
    const res = await request(app)
      .post("/user/updateProfile")
      .send({
        familyName: `new Family Name`,
        givenName: `new Given Name`,
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.data).toEqual(`User modified.`);
    expect(res.status).toEqual(200);
  });
  it("Should not update the user picture because there is no JWT token.", async () => {
    const res = await request(app).post("/user/updateProfile").send({
      familyName: `new Family Name`,
      givenName: `new Given Name`,
    });
    expect(res.body.message).toEqual(
      "No authorization bearer token header is set."
    );
    expect(res.status).toEqual(401);
  });
  it("Should not update the user profile because the familyName is missing.", async () => {
    const res = await request(app)
      .post("/user/updateProfile")
      .send({
        givenName: `new Given Name`,
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.data.familyName.msg).toEqual(`is missing.`);
    expect(res.status).toEqual(400);
  });
  it("Should not update the user profile because the familyName is not long enough.", async () => {
    const res = await request(app)
      .post("/user/updateProfile")
      .send({
        familyName: `new`,
        givenName: `new Given Name`,
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.data.familyName.msg).toEqual(`is missing.`);
    expect(res.status).toEqual(400);
  });
});

describe("POST /user/picture", () => {
  it("Should update the user picture.", async () => {
    const filePath = `${__dirname}/environment/data/validePicture.jpg`;
    const res = await request(app)
      .post("/user/picture")
      .set("Authorization", `Bearer ${jwtToken}`)
      .attach("file", filePath);
    expect(res.body.data.length).toBeGreaterThan(30);
    expect(res.status).toEqual(200);
  });
  it("Should not update the user picture because the picture is missing.", async () => {
    const res = await request(app)
      .post("/user/picture")
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.data.file.msg).toEqual(`Please only upload valid images.`);
    expect(res.status).toEqual(400);
  });
  it("Should not update the user picture because the file is not an image.", async () => {
    const filePath = `${__dirname}/environment/data/invalidDocument.pdf`;
    const res = await request(app)
      .post("/user/picture")
      .set("Authorization", `Bearer ${jwtToken}`)
      .attach("file", filePath);

    expect(res.body.data.file.msg).toEqual(`Please only upload valid images.`);
    expect(res.status).toEqual(400);
  });
  it("Should not update the user picture because the image is invalid (blank image).", async () => {
    const filePath = `${__dirname}/environment/data/invalidPicture.png`;
    const res = await request(app)
      .post("/user/picture")
      .set("Authorization", `Bearer ${jwtToken}`)
      .attach("file", filePath);
    expect(res.body.data.file.msg).toEqual(`Please only upload valid images.`);
    expect(res.status).toEqual(400);
  });
});

describe("POST /user/refreshToken", () => {
  it("Should not refresh the token because the JWT token is missing.", async () => {
    const res = await request(app).post("/user/refreshToken");
    expect(res.body.message).toEqual(
      "No authorization bearer token header is set."
    );
    expect(res.status).toEqual(401);
  });
  it("Should not refresh the token because the JWT token is invalid.", async () => {
    const badToken: string = `fakeJWT`;
    const res = await request(app)
      .post("/user/refreshToken")
      .set("Authorization", `Bearer ${badToken}`);
    expect(res.body.message).toEqual("Invalid authentication token.");
    expect(res.status).toEqual(401);
  });
  it("Should refresh the JWT token.", async () => {
    const res = await request(app)
      .post("/user/refreshToken")
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.data.length).toBeGreaterThan(10);
    expect(res.status).toEqual(200);
  });
});
