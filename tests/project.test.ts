import { app } from "../src/server";
import request from "supertest";

import { config } from "./environment/setup.jest";
const {
  jwtToken,
  projectId,
  projectIsNotAuthorId,
  projectIsNotInId,
  userId,
  userToInviteId,
  projectUser2NotInvited,
  jwtToken2,
} = config;

describe("POST /project", () => {
  it("Should not create the project because the JWT token is invalid ", async () => {
    const badToken: string = `fakeJWT`;
    const res = await request(app)
      .post("/project")
      .send({
        name: "Project test",
        description: "Projet pour les tests",
      })
      .set("Authorization", `Bearer ${badToken}`);
    expect(res.body.message).toEqual("Invalid authentication token.");
    expect(res.status).toEqual(401);
  });
  it("Should not create the project because description is missing", async () => {
    const res = await request(app)
      .post("/project")
      .send({
        name: "Project test",
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.data.description.msg).toEqual("is missing");
    expect(res.status).toEqual(400);
  });
  it("Should create the project.", async () => {
    const res = await request(app)
      .post("/project")
      .send({
        name: "Project test",
        description: "Projet pour les tests",
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.status).toEqual(200);
  });
});

describe("GET /project", () => {
  it("Should not retrieve the projects because the JWT token is invalid ", async () => {
    const badToken: string = `fakeJWT`;
    const res = await request(app) //+65
      .get("/project")
      .set("Authorization", `Bearer ${badToken}`);
    expect(res.body.message).toEqual("Invalid authentication token.");
    expect(res.status).toEqual(401);
  });
  it("Should get the projects.", async () => {
    const res = await request(app)
      .get("/project")
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.data).toBeDefined();
    expect(res.status).toEqual(200);
  });
});

describe("POST /project/invitation", () => {
  it("Should not create the invitation because the JWT token is invalid ", async () => {
    const badToken: string = `fakeJWT`;
    const res = await request(app)
      .post("/project/invitation")
      .send({
        projectId: projectId,
        userToInvite: userToInviteId,
      })
      .set("Authorization", `Bearer ${badToken}`);
    expect(res.body.message).toEqual("Invalid authentication token.");
    expect(res.status).toEqual(401);
  });
  it("Should not create the invitation because the project don't exists.", async () => {
    const fakeProject: string = `fakeProject`;
    const res = await request(app)
      .post("/project/invitation")
      .send({
        projectId: fakeProject,
        userToInvite: userToInviteId,
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.message).toEqual("Could not invite the user.");
    expect(res.status).toEqual(500);
  });
  it("Should not create the invitation because the user don't exists.", async () => {
    const fakeUser: string = `fakeUser`;
    const res = await request(app)
      .post("/project/invitation")
      .send({
        projectId: projectId,
        userToInvite: fakeUser,
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.message).toEqual("Could not invite the user.");
    expect(res.status).toEqual(500);
  });
  it("Should not create the invitation because the user is already in.", async () => {
    const res = await request(app)
      .post("/project/invitation")
      .send({
        projectId: projectId,
        userToInvite: userId,
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.message).toEqual("Could not invite the user.");
    expect(res.status).toEqual(500);
  });
  it("Should not create the invitation because the userToInvite is missing.", async () => {
    const res = await request(app)
      .post("/project/invitation")
      .send({
        projectId: projectId,
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.data.userToInvite.msg).toEqual("is missing");
    expect(res.status).toEqual(400);
  });
  it("Should not create the invitation because the user is not in the project.", async () => {
    const res = await request(app)
      .post("/project/invitation")
      .send({
        projectId: projectIsNotInId,
        userToInvite: userToInviteId,
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.message).toEqual("Could not invite the user.");
    expect(res.status).toEqual(500);
  });
  it("Should not create the invitation because the user is not the author.", async () => {
    const res = await request(app)
      .post("/project/invitation")
      .send({
        projectId: projectIsNotAuthorId,
        userToInvite: userToInviteId,
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.message).toEqual("Could not invite the user.");
    expect(res.status).toEqual(500);
  });
  it("Should create the invitation.", async () => {
    const res = await request(app)
      .post("/project/invitation")
      .send({
        projectId: projectId,
        userToInvite: userToInviteId,
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.data.projectId).toEqual(projectId);
    expect(res.status).toEqual(200);
  });
});

/**
 * Case:
 *  - is already in project
 */
describe("POST /project/accept", () => {
  it("Should not accept the invitation because the JWT token is invalid ", async () => {
    let fakeToken: string = `fakeToken`;
    const res = await request(app)
      .post("/project/invitation/accept")
      .send({
        projectId: projectId,
      })
      .set("Authorization", `Bearer ${fakeToken}`);
    expect(res.body.message).toEqual("Invalid authentication token.");
    expect(res.status).toEqual(401);
  });
  it("Should not accept the invitation because the projectId is missing.", async () => {
    const res = await request(app)
      .post("/project/invitation/accept")
      .send({})
      .set("Authorization", `Bearer ${jwtToken2}`);
    expect(res.body.data.projectId.msg).toEqual("is missing");
    expect(res.status).toEqual(400);
  });
  it("Should not accept the invitation because the projectId doesn't exists.", async () => {
    const fakeProjectId: string = `fakeProjectId`;
    const res = await request(app)
      .post("/project/invitation/accept")
      .send({
        projectId: fakeProjectId,
      })
      .set("Authorization", `Bearer ${jwtToken2}`);
    expect(res.body.message).toEqual("Could not accept the invitation.");
    expect(res.status).toEqual(500);
  });
  it("Should not accept the invitation because the user is not invited.", async () => {
    const res = await request(app)
      .post("/project/invitation/accept")
      .send({
        projectId: projectUser2NotInvited,
      })
      .set("Authorization", `Bearer ${jwtToken2}`);
    expect(res.body.message).toEqual("Could not accept the invitation.");
    expect(res.status).toEqual(500);
  });
  it("Should not accept the invitation because the user is already in the project.", async () => {
    const res = await request(app)
      .post("/project/invitation/accept")
      .send({
        projectId: projectId,
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.message).toEqual("Could not accept the invitation.");
    expect(res.status).toEqual(500);
  });
  it("Should accept the invitation.", async () => {
    const res = await request(app)
      .post("/project/invitation/accept")
      .send({
        projectId: projectId,
      })
      .set("Authorization", `Bearer ${jwtToken2}`);
    expect(res.body.data.projectId).toEqual(projectId);
    expect(res.status).toEqual(200);
  });
});
