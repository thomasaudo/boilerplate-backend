import { app } from "../src/server";
import request from "supertest";
import { config } from "./environment/setup.jest";
const { jwtToken, projectId, projectIsNotInId } = config;

describe("POST /project/message", () => {
  it("Should not post the message because the JWT token is invalid ", async () => {
    const badToken: string = `fakeJWT`;
    const res = await request(app)
      .post("/project/message")
      .send({
        message: "Project test",
        projectId: projectId,
      })
      .set("Authorization", `Bearer ${badToken}`);
    expect(res.body.message).toEqual("Invalid authentication token.");
    expect(res.status).toEqual(401);
  });
  it("Should not post the message because the project doesn't exists", async () => {
    const badProjectId: string = `fakeProjectId`;
    const res = await request(app)
      .post("/project/message")
      .send({
        message: "Message de test",
        projectId: badProjectId,
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.message).toEqual(
      `Could not add the message to the project.`
    );
    expect(res.status).toEqual(500);
  });
  it("Should not post the message because the message is missing.", async () => {
    const res = await request(app)
      .post("/project/message")
      .send({
        projectId: projectId,
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.status).toEqual(400);
  });
  it("Should not post the message because the user is not in the project.", async () => {
    const res = await request(app)
      .post("/project/message")
      .send({
        message: "Message de test",
        projectId: projectIsNotInId,
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.status).toEqual(500);
  });
  it("Should post the message.", async () => {
    const res = await request(app)
      .post("/project/message")
      .send({
        message: "Projet pour les tests",
        projectId: projectId,
      })
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.data.message).toEqual("Projet pour les tests");
    expect(res.status).toEqual(200);
  });
});

describe("GET /project/message", () => {
  it("Should not retrieve the messages because the JWT token is invalid ", async () => {
    const badToken: string = `fakeJWT`;
    const res = await request(app)
      .get(`/project/message/${projectId}`)
      .set("Authorization", `Bearer ${badToken}`);
    expect(res.body.message).toEqual("Invalid authentication token.");
    expect(res.status).toEqual(401);
  });
  it("Should not retrieve the messages because the user is not in the project.", async () => {
    const res = await request(app)
      .get(`/project/message/${projectIsNotInId}`)
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.message).toEqual(`Could not retrieve the messages.`);
    expect(res.status).toEqual(500);
  });
  it("Should retrieve the messages.", async () => {
    const res = await request(app)
      .get(`/project/message/${projectId}`)
      .set("Authorization", `Bearer ${jwtToken}`);
    expect(res.body.data).toBeDefined();
    expect(res.status).toEqual(200);
  });
});
