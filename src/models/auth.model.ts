import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import Boom from "@hapi/boom";

import { IUser, IUserToCreate, IUserToLogin } from "../types/user.type";

import { User } from "../db/user.db";
import { IGoogleProfile } from "../types/social.type";

const generateToken = (user: IUser) => {
  return jwt.sign(
    {
      _id: user._id,
      givenName: user.givenName,
      email: user.email,
    },
    process.env.JWT_SECRET,
    { expiresIn: "30d" }
  );
};

const register = async (user: IUserToCreate) => {
  try {
    user.password = await bcrypt.hash(user.password, 10);
    const doc: IUser = await User.create(user);
    const token: string = generateToken(doc);
    return {
      user: doc,
      token: token
    };
  } catch (err) {
    err.msg = `Could not create the user.`;
    throw Boom.internal(err);
  }
};

const registerGoogleAccount = async (user: IGoogleProfile) => {
  try {
    const doc: IUser = await User.create({
      ...user,
      provider: `google`,
    });
    const token: string = generateToken(doc);
    return {
      token: token,
      user: doc
    };
  } catch (err) {
    err.msg = `Could not create the user.`;
    throw Boom.internal(err);
  }
};

const login = async (user: IUserToLogin) => {
  try {
    const doc = await User.findOne()
      .where({ email: user.email })
      .select(`+password`);
    if (!doc) {
      throw Boom.unauthorized(`Invalid email or password`);
    }

    const isValidPassword = await bcrypt.compare(user.password, doc.password);
    if (!isValidPassword) {
      throw Boom.unauthorized(`Invalid email or password`);
    }

    const token = generateToken(doc);

    return {
      token: token,
      user: doc
    };
  } catch (err) {
    err.msg = `Could not login the user.`;
    throw Boom.internal(err);
  }
};

const loginGoogleAccount = async (user: IGoogleProfile) => {
  try {
    const doc: IUser = await User.findOneAndUpdate(
      {
        email: user.email,
        provider: `google`,
      },
      {
        givenName: user.givenName,
        familyName: user.familyName,
      }
    );
    const token: string = generateToken(doc);
    return {
      token: token,
      user: doc
    };
  } catch (err) {
    err.msg = `Could not login the user.`;
    throw Boom.internal(err);
  }
};

export {
  login,
  register,
  generateToken,
  registerGoogleAccount,
  loginGoogleAccount,
};
