import Boom from "@hapi/boom";
import bcrypt from "bcrypt";

import { IPublicUser, IUser } from "../types/user.type";
import { User } from "../db/user.db";

const deleteUser = async (userEmail: string) => {
  try {
    let userDeleted: IUser = await User.findOneAndDelete({ email: userEmail });
    return userDeleted;
  } catch (err) {
    err.msg = `Could not delete the user.`;
    throw Boom.internal(err);
  }
};

const updateUserPicture = async (pictureUrl: string, userId: string) => {
  try {
    let userModified: IUser = await User.findOneAndUpdate(
      { _id: userId },
      { profilePicture: pictureUrl },
      { new: true }
    );
    return userModified;
  } catch (err) {
    err.msg = `Could not update the user profile picture.`;
    throw Boom.internal(err);
  }
};

const updateProfile = async (propsToChange: any, userId: string) => {
  try {
    let userModified: IUser = await User.findOneAndUpdate(
      { _id: userId },
      propsToChange
    );
    return userModified;
  } catch (err) {
    err.msg = `Could not update the user profile.`;
    throw Boom.internal(err);
  }
};

const changePassword = async (
  userId: string,
  oldPassword: string,
  newPassword: string
) => {
  try {
    let user: IUser = await User.findById(userId).select(`+password`);
    const isValidPassword = await bcrypt.compare(oldPassword, user.password);

    if (!isValidPassword) {
      throw Boom.unauthorized(`Could not update the user password.`);
    }

    await User.findByIdAndUpdate(userId, {
      password: await bcrypt.hash(newPassword, 10),
    });
  } catch (err) {
    err.msg = `Could not update the user password.`;
    throw Boom.internal(err);
  }
};

const search = async (search: string) => {
  try {

      const publicSearchUsers: IPublicUser[] = [];
      const searchUsers: IUser[] = await User.find({ email: { $regex: search } }).limit(5);
      searchUsers.forEach(u => publicSearchUsers.push({
        familyName: u.familyName,
        givenName: u.givenName,
        email: u.email,
        profilePicture: u.profilePicture,
        _id: u._id
      }))

      return publicSearchUsers;
  } catch (err) {
    throw Boom.internal(`Can't get users.`);
  }
}

export { deleteUser, updateUserPicture, changePassword, updateProfile, search };
