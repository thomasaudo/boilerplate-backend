import Boom from "@hapi/boom";
import { Project } from "../db/project.db";
import { IMessage } from "../types/project.type";
import { isInProject } from "./project.model";

const add = async (message: string, projectId: string, userId: string) => {
  try {
    let _isInProject: boolean = await isInProject(projectId, userId);
    if (_isInProject) {
      await Project.findByIdAndUpdate(projectId, {
        $push: {
          messages: {
            user: userId,
            message: message,
            createdDate: new Date(),
          },
        },
      }).exec();
      return { message, projectId };
    } else {
      throw Boom.internal(`User is not in the project.`);
    }
  } catch (err) {
    throw Boom.internal(`Could not add the message to the project.`);
  }
};

const get = async (projectId: string, userId: string) => {
  try {
    let _isInProject: boolean = await isInProject(projectId, userId);
    if (_isInProject) {
      const messages: IMessage[] = (await Project.findById(projectId)).messages;
      return messages;
    } else {
      throw Boom.internal(`User is not in the project.`);
    }
  } catch (err) {
    throw Boom.internal(`Could not retrieve the messages.`);
  }
};

export { add, get };
