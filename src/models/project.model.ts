import Boom from "@hapi/boom";

import { IPublicUser, IUser } from "../types/user.type";
import { IProject, IProjectToCreate } from "../types/project.type";

import { User } from "../db/user.db";
import { Project } from "../db/project.db";

const isInvited = async (projectId: string, userId: string) => {
  try {
    const projects: IUser[] = await User.find({
      _id: userId,
      "invitations.project": projectId,
    });
    return projects.length > 0 ? true : false;
  } catch (err) {
    throw Boom.internal(`Could not find if the user is invited.`);
  }
};

const isInProject = async (projectId: string, userId: string) => {
  try {
    const project: IUser = await User.findOne({
      _id: userId,
      projects: projectId,
    });
    return project ? true : false;
  } catch (err) {
    throw Boom.internal(`Could not find if the user is in the project.`);
  }
};

const isAuthor = async (projectId: string, userId: string) => {
  try {
    const project: IProject = await Project.findOne({
      author: userId,
      _id: projectId,
    });
    return project ? true : false;
  } catch (err) {
    throw Boom.internal(`Could not find if the user is the author.`);
  }
};

const create = async (project: IProjectToCreate, userId: string) => {
  try {
    const doc: IProject = await Project.create({
      ...project,
      users: [userId],
      author: userId,
    });
    await User.findByIdAndUpdate(userId, {
      $push: {
        projects: doc._id,
      },
    }).exec();
    return doc;
  } catch (err) {
    throw Boom.internal(`Could not create the project.`);
  }
};

const invite = async (
  projectId: string,
  userToInvite: string,
  userId: string
) => {
  try {
    let _isInProject: boolean = await isInProject(projectId, userToInvite);
    if (_isInProject) {
      throw Boom.internal(`Could not invite the user.`);
    }

    let _isAuthor: boolean = await isAuthor(projectId, userId);
    if (!_isAuthor) {
      throw Boom.internal(`Could not invite the user.`);
    }

    await User.findByIdAndUpdate(userToInvite, {
      $push: {
        invitations: {
          project: projectId,
          inviteFrom: userId,
        },
      },
    }).exec();
    return { projectId };
  } catch (err) {
    throw Boom.internal(`Could not invite the user.`);
  }
};

const join = async (projectId: string, userId: string) => {
  try {
    await Project.findByIdAndUpdate(projectId, {
      $push: {
        users: userId,
      },
    }).exec();
    await User.findByIdAndUpdate(userId, {
      $push: {
        projects: projectId,
      },
    }).exec();
    await deleteInvitations(projectId, userId);
    return { projectId, userId };
  } catch (err) {
    throw Boom.internal(`Could not join the project.`);
  }
};

const deleteInvitations = async (projectId: string, userId: string) => {
  try {
    User.findByIdAndUpdate(userId, {
      $pull: { invitations: { project: projectId } },
    }).exec();
  } catch (err) {
    throw Boom.internal(`Could not delete the invitation.`);
  }
};

const accept = async (projectId: string, userId: string) => {
  try {
    let isUserInvited: boolean = await isInvited(projectId, userId);
    let isUserInProject: boolean = await isInProject(projectId, userId);
    if (isUserInvited && !isUserInProject) {
      await join(projectId, userId);
      return { projectId };
    } else {
      throw Boom.unauthorized("User is not invited.");
    }
  } catch (err) {
    throw Boom.internal(`Could not accept the invitation.`);
  }
};

const getUserProjects = async (userId: string) => {
  try {
    const projects: IProject[] = await Project.find({ users: userId });
    return projects;
  } catch (err) {
    throw Boom.internal(`Can't get user projects.`);
  }
};

const getProjectInformation = async (userId: string, projectId: string) => {
  try {
    let isUserInProject: boolean = await isInProject(projectId, userId);
    if (isUserInProject) {

      const publicProjectUsers: IPublicUser[] = [];
      const projectUsers: IUser[] = await User.find({projects: projectId})
      projectUsers.forEach(u => publicProjectUsers.push({
        familyName: u.familyName,
        givenName: u.givenName,
        profilePicture: u.profilePicture,
        _id: u._id
      }))

      const project: IProject = await Project.findById(projectId); 
      return {project, users: publicProjectUsers};
    } else {
      throw Boom.unauthorized("User is not in project.");
    }
  } catch (err) {
    throw Boom.internal(`Can't get informations about the project`);
  }
}

export { accept, invite, create, getUserProjects, isInProject, getProjectInformation };
