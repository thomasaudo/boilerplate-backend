import * as util from "util";

import { IGoogleProfile } from "../types/social.type";
import { User } from "../db";
import { IUser } from "../types/user.type";
import { googleStorage } from "../utils";

const googleAccountExist = async (user: IGoogleProfile) => {
  let doc: IUser = await User.findOne({
    email: user.email,
    provider: `google`,
  });
  return doc != undefined ? true : false;
};

const uploadImageToGoogle = (file: any, userId: string): Promise<string> =>
  new Promise(async (resolve, reject) => {
    const storage = await googleStorage();
    const bucket = storage.bucket(process.env.GCP_PICTURE_BUCKET);

    const { originalname, buffer } = file;
    const blob = bucket.file(`${userId}${originalname.replace(/ /g, "_")}`);
    const blobStream = blob.createWriteStream({
      resumable: false,
    });

    blobStream
      .on("finish", () => {
        const publicUrl = util.format(
          `https://storage.googleapis.com/${bucket.name}/${blob.name}`
        );
        resolve(publicUrl);
      })
      .on("error", () => {
        reject(`Unable to upload the user profile picture`);
      })
      .end(buffer);
  });

export { googleAccountExist, uploadImageToGoogle };
