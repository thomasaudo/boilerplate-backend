import { Document } from "mongoose";

interface IProject extends Document {
  author: string;
  description: string;
  name: string;
  createdDate: Date;
  users: [string];
  messages: [IMessage];
}

interface IMessage {
  user: string;
  message: string;
  createdDate: Date;
}

interface IProjectToCreate {
  description: string;
  name: string;
}

export { IProject, IProjectToCreate, IMessage };
