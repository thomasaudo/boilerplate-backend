import { Document } from "mongoose";

interface IUser extends Document {
  familyName: string;
  givenName: string;
  email: string;
  profilePicture?: string;
  password?: string;
  _id?: string;
  provider?: string;
  projects: [string];
  invitations: [
    {
      project: string;
      inviteFrom: string;
    }
  ];
}

interface IUserToCreate {
  familyName: string;
  givenName: string;
  email: string;
  password: string;
}

interface IUserToLogin {
  email: string;
  password: string;
}

interface IPublicUser {
  familyName: string;
  givenName: string;
  email?: string;
  profilePicture?: string;
  _id: string;
}

export { IUser, IUserToLogin, IUserToCreate, IPublicUser };
