import { Request } from "express";
import { IUser } from "./user.type";

interface AuthReq extends Request {
  session: any;
  userDoc?: IUser;
}

export { AuthReq };
