interface IGoogleProfile {
  familyName: string;
  profilePicture: string;
  givenName: string;
  email: string;
}

export { IGoogleProfile };
