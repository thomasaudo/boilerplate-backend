import mongoose, { Schema } from "mongoose";
import { IProject } from "../types/project.type";

const ProjectSchema: Schema = new Schema({
  author: {
    ref: "User",
    type: Schema.Types.ObjectId,
    required: true,
  },
  __v: { type: Number, select: false },
  name: { type: String, required: true },
  description: { type: String, required: true },
  createdDate: {
    type: Date,
    default: () => new Date(),
  },
  users: [
    {
      ref: "User",
      type: Schema.Types.ObjectId,
    },
  ],
  messages: [
    {
      user: {
        ref: "User",
        type: Schema.Types.ObjectId,
      },
      message: { type: String, required: true },
      createdDate: {
        type: Date,
        default: () => new Date(),
      },
    },
  ],
});

const Project = mongoose.model<IProject>("Project", ProjectSchema);

export { Project };
