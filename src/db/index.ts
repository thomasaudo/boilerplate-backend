import mongoose from "mongoose";

const mongoConnect = async () => {
  let URI: string;

  if (process.env.LOGNAME == `travis`) {
    URI = unescape(process.env.MONGO_URI);
  } else {
    URI = process.env.MONGO_URI;
  }

  await mongoose.connect(URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });
};

const mongoDisconnect = async () => {
  await mongoose.disconnect();
};

/** 
mongoose.connection.on("close", () => {
  console.log("🍂 Connection Closed");
});

mongoose.connection.on("error", (error) => {
  throw new Error(error.message);
});

mongoose.connection.on("connected", () => {
  console.log("🍃 Connection Established");
});

mongoose.connection.on("reconnected", () => {
  console.log("🍃 Connection Reestablished");
});

mongoose.connection.on("disconnected", () => {
  console.log("🍂 Connection Disconnected");
});
*/
export { mongoConnect, mongoDisconnect };

export * from "./user.db";
