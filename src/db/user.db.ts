import mongoose, { Schema } from "mongoose";
import Boom from "@hapi/boom";

import { IUser } from "../types/user.type";

const UserSchema: Schema = new Schema({
  password: { type: String, select: false, required: false },
  familyName: { type: String, required: true },
  givenName: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  profilePicture: { type: String, required: false, unique: false, default: process.env.DEFAULT_PICTURE },
  __v: { type: Number, select: false },
  provider: { type: String, required: false },
  joinedDate: {
    type: Date,
    default: () => new Date(),
  },
  projects: [
    {
      ref: "Project",
      type: Schema.Types.ObjectId,
    },
  ],
  invitations: [
    {
      project: {
        ref: "Project",
        type: Schema.Types.ObjectId,
      },
      inviteFrom: {
        ref: "User",
        type: Schema.Types.ObjectId,
      },
    },
  ],
});

UserSchema.post(
  "save",
  (error: any, doc: mongoose.Document, next: Function) => {
    if (error.name === "MongoError" && error.code === 11000) {
      next(
        Boom.conflict(undefined, {
          email: {
            param: "email",
            msg: "is already used.",
          },
        })
      );
    } else {
      next(error);
    }
  }
);

const User = mongoose.model<IUser>("User", UserSchema);

export { User };
