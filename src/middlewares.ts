import boom from "@hapi/boom";
import jwt from "jsonwebtoken";
import { NextFunction, RequestHandler } from "express-serve-static-core";
import { AuthReq } from "./types";
import { User } from "./db";
import { IUser } from "./types/user.type";

const errorHandler = (err: any, req: any, res: any, next: any) => {
  if (!err.isBoom) {
    if (err instanceof SyntaxError && err.hasOwnProperty("body"))
      err = boom.badRequest(err.message);
    else if (err.name === "UnauthorizedError") err = boom.unauthorized(err);
    else err = boom.internal(err);
  }

  const errPayload = err.output.payload;

  res.status(errPayload.statusCode).json({
    message: err.message || errPayload.message,
    data: err.data || undefined,
  });

  next();
};

const mustBeAuthentificated = (): RequestHandler => async (req, res, next) => {
  try {
    const bearerToken = req.headers.authorization;
    if (!bearerToken?.startsWith("Bearer"))
      throw boom.unauthorized("No authorization bearer token header is set.");

    try {
      (req as AuthReq).session = jwt.verify(
        bearerToken.split(" ")[1],
        process.env.JWT_SECRET
      );
    } catch (err) {
      throw boom.unauthorized("Invalid authentication token.");
    }
    next();
  } catch (error) {
    next(error);
  }
};

const injectUserDoc = (): RequestHandler => async (req: AuthReq, res, next) => {
  try {
    const _id = req.session?._id;
    if (!_id) {
      throw boom.unauthorized("You must be login");
    }

    const userDoc: IUser = await User.findById(_id);

    // If the user was deleted but the JWT is still valid.
    if (userDoc === null || userDoc === undefined) {
      throw boom.unauthorized("User don't exists");
    }

    req.userDoc = userDoc;
    next();
  } catch (error) {
    next(error);
  }
};

export { errorHandler, mustBeAuthentificated, injectUserDoc };
