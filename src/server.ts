import express from "express";
import * as bodyParser from "body-parser";
import * as dotenv from "dotenv";
import cors from 'cors';
import multer from "multer";
import Helmet from "helmet";

import { router } from "./routes";
import { mongoConnect } from "./db";
import { errorHandler } from "./middlewares";

const app: express.Application = express();

dotenv.config();


app.use(Helmet());
app.use(cors());

const multerMid = multer({
  storage: multer.memoryStorage(),
  limits: {
    fileSize: 5 * 1024 * 1024,
  },
});

app.use(
  bodyParser.json({
    limit: "50mb",
    verify(req: any, res, buf, encoding) {
      req.rawBody = buf;
    },
  })
);

app.use(multerMid.single("file"));

app.use(router);

app.use(errorHandler);

const initServer = async () => {
  await mongoConnect();
  new Promise((resolve) =>
    app.listen(process.env.PORT, () => {
      console.log(
        `⚡ Server is listening on http://localhost:${process.env.PORT}`
      );
      resolve(app);
    })
  );
};

export { initServer, app };
