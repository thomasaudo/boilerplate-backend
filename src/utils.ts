import { validationResult } from "express-validator";
import { Storage } from "@google-cloud/storage";
import { check } from "express-validator";
import { OAuth2Client } from "google-auth-library";

const validateInput = (req: any) => {
  return validationResult(req);
};

const googleConnection = async () => {
  return new OAuth2Client(
    process.env.GOOGLE_CLIENT_ID,
    process.env.GOOGLE_CLIENT_SECRET,
    process.env.GOOGLE_REDIRECT_URL
  );
};

const googleStorage = async () => {
  return new Storage({
    keyFilename: process.env.GCP_SERVICE_FILE,
    projectId: process.env.GCP_PROJECT_ID,
  });
};

const imageValidator = () => {
  return check("file")
    .custom((value, { req }) => {
      if (req.file === undefined) return false;
      if (req.file.size < 10) return false;
      if (!req.file.mimetype.includes("image")) return false;
      return true;
    })
    .withMessage("Please only upload valid images.");
};

export { validateInput, googleConnection, googleStorage, imageValidator };
