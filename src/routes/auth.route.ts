import { Router } from "express";
import { body } from "express-validator";

import {
  handleGoogleCallback,
  handleGoogleLogin,
} from "../controllers/google.controller";
import { loginUser, registerUser } from "../controllers/auth.controller";

const authRouter: Router = Router();

/**
 * @api {POST} /user/login Login the user
 * @apiName LoginUser
 * @apiGroup User
 *
 * @apiParam {String} email user's email.
 * @apiParam {String} password user's password.
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *       "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MDM0MGNhYThjYmM5N2Q3NDRkNzMwNzYiLCJuYW1lIjoidGVzdEEiLCJlbWFpbCI6InRlc3RhQGdtYWlsLmNvbSIsImlhdCI6MTYxNDAyMzkyMCwiZXhwIjoxNjE2NjE1OTIwfQ.nD8RRVBDfzDkzV5Xfq5VYB4iE43tO0Ir6ZdJr2W9alg",
 *       "email": "testa@gmail.com",
 *       "name": "testA"
 *   }
 * }
 */
authRouter.post(
  "/login",
  body(["email", "password"], "is missing").exists({ checkFalsy: true }),
  body("email", "is not a valid email address.").isEmail(),
  loginUser
);

/**
 * @api {POST} /user/register Register the user
 * @apiName RegisterUser
 * @apiGroup User
 *
 * @apiParam {String} email user's email.
 * @apiParam {String} password user's password.
 * @apiParam {String} name user's password.
 * @apiParam {Number} age user's password.
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *       "_id": "60340d6ec3eb66d480390be2",
 *       "email": "testd@gmail.com",
 *       "name": "test test",
 *   }
 * }
 */
authRouter.post(
  "/register",
  body(["email", "familyName", "givenName"], "is missing.").exists({
    checkFalsy: true,
  }),
  body("email", "is not a valid email address.").isEmail(),
  body("password", "must be of 6 characters length minimum.")
    .exists({ checkFalsy: true })
    .isLength({ min: 6 }),
  registerUser
);

/**
 * @api {GET} /user/google/url Get the Auth Google URL
 * @apiName handleGoogleLogin
 * @apiGroup User

 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "data": "https://accounts.google.com/o/oauth2/v2/auth?access_type=offline&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&response_type=code&client_id=1064190439886-f0gmpni5pro2atcsgku3m4lajt1ufs61.apps.googleusercontent.com&redirect_uri=http%3A%2F%2Flocalhost%3A3000%2Fgoogle%2Fcallback"
 * }
 */
authRouter.get("/google/url", handleGoogleLogin);

/**
 * @api {POST} /user/google/code Login / Register the googleAccount from the googleCode
 * @apiName handleGoogleCallback
 * @apiGroup User
 *
 * @apiParam {String} googleCode user's facebook code.
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *       "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MDM1NTQ2MGRkNDNjM2YxZjRhYjAwNmUiLCJnaXZlbk5hbWUiOiJUaG9tYXMiLCJlbWFpbCI6InRob21hc2F1ZG85QGdtYWlsLmNvbSIsImlhdCI6MTYxNDExNDAzNSwiZXhwIjoxNjE2NzA2MDM1fQ.-dTSHDIP9VXRbjHmmd-NN3HQYV7b7L1FCLD1prmZnIg",
 *       "email": "thomasaudo9@gmail.com",
 *       "givenName": "Thomas"
 *   }
 * }
 */
authRouter.post(
  "/google/code",
  body("googleCode", "is missing.").exists({ checkFalsy: true }),
  handleGoogleCallback
);

export { authRouter };
