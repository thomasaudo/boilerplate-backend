import { Router } from "express";
import { injectUserDoc, mustBeAuthentificated } from "../middlewares";
import { body } from "express-validator";

import { getMessages, addMessage } from "../controllers/chat.controller";

const chatRouter: Router = Router();

/**
 * @api {POST} /project/message Add the message to the project.
 * @apiName addMessage
 * @apiGroup Project
 *
 * @apiParam {String} message the message you want to add to the project.
 * @apiParam {String} projectId the selected project.
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *       "message": "API documentation",
 *       "projectId": "603410705e1aa2c3ec153050"
 *   }
 * }
 */
chatRouter.post(
  "/message",
  body(["message", "projectId"], "is missing").exists({ checkFalsy: true }),
  mustBeAuthentificated(),
  injectUserDoc(),
  addMessage
);

/**
 * @api {GET} /project/message/:id Get all the message(s) of the selected project.
 * @apiName getMessage
 * @apiGroup Project
 *
 * @apiParam {String} id The project Id.
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": [
 *       {
 *           "_id": "6034107b5e1aa2c3ec153051",
 *           "user": "60340d6ec3eb66d480390be2",
 *           "message": "API documentation",
 *           "createdDate": "2021-02-22T20:13:47.799Z"
 *       }
 *   ]
 * }
 */
chatRouter.get(
  "/message/:projectId",
  mustBeAuthentificated(),
  injectUserDoc(),
  getMessages
);

export { chatRouter };
