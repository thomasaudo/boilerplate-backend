import { Router } from "express";
import { body, oneOf } from "express-validator";

import { injectUserDoc, mustBeAuthentificated } from "../middlewares";
import {
  getProfile,
  updatePicture,
  updatePassword,
  updateUser,
  refreshToken,
  searchUsers
} from "../controllers/user.controller";
import { imageValidator } from "../utils";

const userRouter: Router = Router();

/**
 * @api {GET} /userProfile Get the user's profile.
 * @apiName getProfile
 * @apiGroup User
 * *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *   ""data": {
 *       "projects": [
 *           "603410705e1aa2c3ec153050"
 *       ],
 *       "_id": "60340d6ec3eb66d480390be2",
 *       "email": "testd@gmail.com",
 *       "name": "test test",
 *       "age": "21",
 *       "invitations": []
 *   }
 * }
 */
userRouter.get(
  `/profile`,
  mustBeAuthentificated(),
  injectUserDoc(),
  getProfile
);

/**
 * @api {POST} /user/picture Update the user's profile picture
 * @apiName updatePicture
 * @apiGroup User
 *
 * @apiParam {File} file user's profile picture.
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": "https://storage.googleapis.com/profile-picture-ns/6036b55f40f01fb3f4292ee1picture.png"
 * }
 */
userRouter.post(
  "/picture",
  imageValidator(),
  mustBeAuthentificated(),
  injectUserDoc(),
  updatePicture
);

/**
 * @api {POST} /user/updatePassword Update the user's password.
 * @apiName updatePassword
 * @apiGroup User
 *
 * @apiParam {String} oldPassword user's current password.
 * @apiParam {String} newPassword user's new password.
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *   "message": "User's password was changed."
 * }
 */
userRouter.post(
  "/updatePassword",
  body(
    ["newPassword", "oldPassword"],
    "must be of 6 characters length minimum."
  )
    .exists({ checkFalsy: true })
    .isLength({ min: 6 }),
  mustBeAuthentificated(),
  injectUserDoc(),
  updatePassword
);

/**
 * @api {POST} /user/updateProfile Update the user's profile.
 * @apiName updateUser
 * @apiGroup User
 *
 * @apiParam {String} givenName user's new givenName.
 * @apiParam {String} familyName user's new familyName.
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *   "message": "User modified."
 * }
 */
userRouter.post(
  "/updateProfile",
  body(["givenName", "familyName"], "is missing.")
    .exists()
    .isLength({ min: 4 }),
  mustBeAuthentificated(),
  injectUserDoc(),
  updateUser
);

/**
 * @api {POST} /user/refreshToken Refresh the user's JWT token.
 * @apiName refreshToken
 * @apiGroup User
 *
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": "XXXXX_NEW_JWT_TOKEN_XXXXXXX"
 * }
 */
userRouter.post(
  "/refreshToken",
  mustBeAuthentificated(),
  injectUserDoc(),
  refreshToken
);

userRouter.get('/searchUsers/:searchString', mustBeAuthentificated(), injectUserDoc(), searchUsers);

export { userRouter };
