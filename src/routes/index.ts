import { Router } from "express";

import { authRouter } from "./auth.route";
import { userRouter } from "./user.route";
import { projectRouter } from "./project.route";
import { chatRouter } from "./chat.route";

const router: Router = Router();

router.use("/user/", authRouter);
router.use("/user/", userRouter);
router.use("/project/", projectRouter);
router.use("/project/", chatRouter);

export { router };
