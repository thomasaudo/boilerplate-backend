import { Router } from "express";
import { injectUserDoc, mustBeAuthentificated } from "../middlewares";
import { body } from "express-validator";

import {
  acceptInvitation,
  createProject,
  getProjects,
  inviteUser,
  getProject
} from "../controllers/project.controller";

const projectRouter: Router = Router();

/**
 * @api {POST} /project Create a project.
 * @apiName createProject
 * @apiGroup Project
 *
 * @apiParam {String} description project's email.
 * @apiParam {String} name project's password.
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *   "data": {
 *       "users": [
 *           "60340d6ec3eb66d480390be2"
 *       ],
 *       "_id": "603412cf5e1aa2c3ec153052",
 *       "name": "test test",
 *       "description": "aya",
 *       "author": "60340d6ec3eb66d480390be2",
 *       "createdDate": "2021-02-22T20:23:43.476Z",
 *       "messages": [],
 *   }
 * }
 */
projectRouter.post(
  "/",
  body(["description", "name"], "is missing").exists({ checkFalsy: true }),
  mustBeAuthentificated(),
  injectUserDoc(),
  createProject
);

/**
 * @api {GET} /project Get all user projects.
 * @apiName getProjects
 * @apiGroup Project
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "data": [
 *       {
 *           "users": [
 *               "60340d6ec3eb66d480390be2"
 *           ],
 *           "_id": "603410705e1aa2c3ec153050",
 *           "name": "test test",
 *           "description": "aya",
 *           "author": "60340d6ec3eb66d480390be2",
 *           "createdDate": "2021-02-22T20:13:36.762Z",
 *           "messages": [
 *               {
 *                   "_id": "6034107b5e1aa2c3ec153051",
 *                   "user": "60340d6ec3eb66d480390be2",
 *                   "message": "API documentation",
 *                   "createdDate": "2021-02-22T20:13:47.799Z"
 *               }
 *           ]
 *       }
 *   ]
 * }
 */
projectRouter.get("/", mustBeAuthentificated(), injectUserDoc(), getProjects);


projectRouter.get("/:projectId", mustBeAuthentificated(), injectUserDoc(), getProject);



/**
 * @api {POST} /project/invitation Create an invitation to a project.
 * @apiName inviteUser
 * @apiGroup Project
 *
 * @apiParam {String} projectId The id of the project.
 * @apiParam {String} userToInvite The id of the user you want to invite.
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "data": {
 *       "projectId": "603410705e1aa2c3ec153050"
 *   }
 * }
 */
projectRouter.post(
  "/invitation",
  body(["projectId", "userToInvite"], "is missing").exists({
    checkFalsy: true,
  }),
  mustBeAuthentificated(),
  injectUserDoc(),
  inviteUser
);

/**
 * @api {POST} /project/invitation/accept Accept an invitation to a project.
 * @apiName acceptInvitation
 * @apiGroup Project
 *
 * @apiParam {String} projectId The id of the project.
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "data": {
 *       "projectId": "603410705e1aa2c3ec153050"
 *   }
 * }
 */
projectRouter.post(
  "/invitation/accept",
  body(["projectId"], "is missing").exists({ checkFalsy: true }),
  mustBeAuthentificated(),
  injectUserDoc(),
  acceptInvitation
);

export { projectRouter };
