import { Response } from "express";
import Boom from "@hapi/boom";
import { NextFunction } from "express-serve-static-core";
import { validateInput } from "../utils";
import { AuthReq } from "../types";
import {
  accept,
  create,
  getUserProjects,
  invite,
  getProjectInformation
} from "../models/project.model";
import { IProjectToCreate } from "../types/project.type";

const createProject = async (
  req: AuthReq,
  res: Response,
  next: NextFunction
) => {
  const err = validateInput(req);
  if (!err.isEmpty()) {
    return next(Boom.badRequest(undefined, err.mapped()));
  }

  const project: IProjectToCreate = req.body;
  try {
    const doc = await create(project, req.session._id);
    return res.json({ data: doc });
  } catch (err) {
    next(err);
  }
};

const inviteUser = async (req: AuthReq, res: Response, next: NextFunction) => {
  const err = validateInput(req);
  if (!err.isEmpty()) {
    return next(Boom.badRequest(undefined, err.mapped()));
  }

  const { projectId, userToInvite } = req.body;
  try {
    const doc = await invite(projectId, userToInvite, req.session._id);
    return res.json({ data: doc });
  } catch (err) {
    next(err);
  }
};

const acceptInvitation = async (
  req: AuthReq,
  res: Response,
  next: NextFunction
) => {
  const err = validateInput(req);
  if (!err.isEmpty()) {
    return next(Boom.badRequest(undefined, err.mapped()));
  }

  const { projectId } = req.body;
  try {
    const doc = await accept(projectId, req.session._id);
    return res.json({ data: doc });
  } catch (err) {
    next(err);
  }
};

const getProjects = async (req: AuthReq, res: Response, next: NextFunction) => {
  try {
    const projects = await getUserProjects(req.session._id);
    return res.json({ data: projects });
  } catch (err) {
    next(err);
  }
};

const getProject = async (req: AuthReq, res: Response, next: NextFunction) => {
  try {
    const { projectId } = req.params;
    const project = await getProjectInformation(req.session._id, projectId);
    return res.json({ data: project });
  } catch (err) {
    next(err);
  }
};

export { createProject, inviteUser, acceptInvitation, getProjects, getProject };
