import express from "express";
import Boom from "@hapi/boom";

import { validateInput } from "../utils";
import { login, register } from "../models/auth.model";
import { IUserToCreate, IUserToLogin } from "../types/user.type";

const loginUser = async (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => {
  const err = validateInput(req);
  if (!err.isEmpty()) {
    return next(Boom.badRequest(undefined, err.mapped()));
  }

  const user: IUserToLogin = req.body;
  try {
    const doc = await login(user);
    return res.json({ data: doc });
  } catch (err) {
    next(err);
  }
};

const registerUser = async (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => {
  const err = validateInput(req);
  if (!err.isEmpty()) {
    return next(Boom.badRequest(undefined, err.mapped()));
  }

  const user: IUserToCreate = req.body;
  try {
    const data = await register(user);
    return res.json({
      data: data
    });
  } catch (err) {
    next(err);
  }
};

export { loginUser, registerUser };
