import { Response } from "express";
import { NextFunction } from "express-serve-static-core";
import Boom from "@hapi/boom";

import { validateInput } from "../utils";

import {
  updateUserPicture,
  changePassword,
  updateProfile,
  search,
} from "../models/user.model";
import { uploadImageToGoogle } from "../models/google.model";

import { AuthReq } from "../types";
import { IPublicUser, IUser } from "../types/user.type";
import { generateToken } from "../models/auth.model";

const getProfile = async (req: AuthReq, res: Response) => {
  const data = req.userDoc;
  res.json({ data });
};

const updatePicture = async (
  req: AuthReq,
  res: Response,
  next: NextFunction
) => {
  const err = validateInput(req);
  if (!err.isEmpty()) {
    return next(Boom.badRequest(undefined, err.mapped()));
  }

  try {
    const pictureUrl: string = await uploadImageToGoogle(
      req.file,
      req.session._id
    );
    const doc: IUser = await updateUserPicture(pictureUrl, req.session._id);
    res.json({ data: doc.profilePicture });
  } catch (err) {
    next(err);
  }
};

const updateUser = async (req: AuthReq, res: Response, next: NextFunction) => {
  const err = validateInput(req);
  if (!err.isEmpty()) {
    return next(Boom.badRequest(undefined, err.mapped()));
  }

  try {
    let propsToChange = {
      familyName: req.body.familyName,
      givenName: req.body.givenName,
    };
    await updateProfile(propsToChange, req.session._id);

    res.json({ data: `User modified.` });
  } catch (err) {
    next(err);
  }
};

const updatePassword = async (
  req: AuthReq,
  res: Response,
  next: NextFunction
) => {
  const err = validateInput(req);
  if (!err.isEmpty()) {
    return next(Boom.badRequest(undefined, err.mapped()));
  }

  try {
    const { oldPassword, newPassword } = req.body;
    await changePassword(req.session._id, oldPassword, newPassword);
    res.json({ data: `User's password was changed.` });
  } catch (err) {
    next(err);
  }
};

const refreshToken = async (
  req: AuthReq,
  res: Response,
  next: NextFunction
) => {
  try {
    const newToken: string = generateToken(req.userDoc);
    res.json({ data: newToken });
  } catch (err) {
    next(err);
  }
};

const searchUsers = async (
  req: AuthReq,
  res: Response,
  next: NextFunction
) => {
  try {
    const { searchString } = req.params;
    const users = await search(searchString);
    res.json({ data: users });
  } catch (err) {
    next(err);
  }
};

export { getProfile, updatePicture, updatePassword, updateUser, refreshToken, searchUsers };
