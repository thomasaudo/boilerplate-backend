import { Request, Response, NextFunction } from "express";
import Boom from "@hapi/boom";
import { googleConnection, validateInput } from "../utils";
import { IGoogleProfile } from "../types/social.type";
import { googleAccountExist } from "../models/google.model";
import {
  loginGoogleAccount,
  registerGoogleAccount,
} from "../models/auth.model";

const handleGoogleLogin = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let authorizeUrl: string;

  try {
    const googleClient = await googleConnection();
    authorizeUrl = googleClient.generateAuthUrl({
      access_type: "offline",
      scope: [
        `https://www.googleapis.com/auth/userinfo.profile`,
        `https://www.googleapis.com/auth/userinfo.email`,
      ],
    });
  } catch (err) {
    next(err);
  }

  res.json({ data: authorizeUrl });
};

const handleGoogleCallback = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const err = validateInput(req);
  if (!err.isEmpty()) {
    return next(Boom.badRequest(undefined, err.mapped()));
  }

  const { googleCode } = req.body;

  try {
    let data;

    const googleClient = await googleConnection();

    const r: any = await googleClient.getToken(unescape(googleCode));
    googleClient.setCredentials(r.tokens);

    const userInfo: any = await googleClient.request({
      url: process.env.GOOGLE_PROFILE_URL,
    });

    const userProfile: IGoogleProfile = {
      familyName: userInfo.data.names[0].familyName,
      profilePicture: userInfo.data.photos[0].url,
      givenName: userInfo.data.names[0].givenName,
      email: userInfo.data.emailAddresses[0].value,
    };

    let userExist: boolean = await googleAccountExist(userProfile);

    if (userExist) {
      data = await loginGoogleAccount(userProfile);
    } else {
      data = await registerGoogleAccount(userProfile);
    }

    res.json({ data: data });
  } catch (err) {
    return next(err);
  }
};

export { handleGoogleCallback, handleGoogleLogin };
