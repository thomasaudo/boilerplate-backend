import { NextFunction } from "express-serve-static-core";
import { Response } from "express";
import { validateInput } from "../utils";
import Boom from "@hapi/boom";
import { AuthReq } from "../types";

import { add, get } from "../models/chat.model";

const addMessage = async (req: AuthReq, res: Response, next: NextFunction) => {
  const err = validateInput(req);
  if (!err.isEmpty()) {
    return next(Boom.badRequest(undefined, err.mapped()));
  }

  const { message, projectId } = req.body;
  try {
    const doc = await add(message, projectId, req.session._id);
    return res.json({ data: doc });
  } catch (err) {
    next(err);
  }
};

const getMessages = async (req: AuthReq, res: Response, next: NextFunction) => {
  const { projectId } = req.params;
  if (!projectId) {
    return next(Boom.badRequest("Project id is missing"));
  }

  try {
    const doc = await get(projectId, req.session._id);
    return res.json({ data: doc });
  } catch (err) {
    next(err);
  }
};

export { getMessages, addMessage };
