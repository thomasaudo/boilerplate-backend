module.exports = {
  clearMocks: true,
  roots: ["<rootDir>/tests"],
  setupFilesAfterEnv: ["<rootDir>/tests/environment/setup.jest.ts"],
  testEnvironment: "node",
  preset: "ts-jest",
};
